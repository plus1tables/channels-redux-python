.. channels-redux documentation master file, created by
   sphinx-quickstart on Wed Sep 19 20:58:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to channels-redux's documentation!
==========================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
