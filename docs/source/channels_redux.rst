channels\_redux
=======================

.. automodule:: channels_redux
    :members:
    :undoc-members:
    :show-inheritance:


.. toctree::
   channels_redux.templatetags
   channels_redux.notify
   channels_redux.rest
   channels_redux.signals
   channels_redux.views

