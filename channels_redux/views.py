import os.path
from typing import List, Union

from django.db.models import QuerySet, Model
from django.views.generic import *

from channels_redux.rest import router


def get_app_name(cls):
    """The name of the app the class belongs to"""
    return cls.__module__.split('.')[0]


def redux_objects(key_field, *model_object_pairs):
    """Builds a dictionary from ModelName -> Object[key_field] -> Object"""
    object_state = {}
    for pair in model_object_pairs:
        model_name, serialized_objects = pair
        existing = object_state.get(model_name, {})
        object_state[model_name] = {
            **existing,
            **key_to_object_dict(key_field, serialized_objects)
        }
    return object_state


def key_to_object_dict(key_field, serialized_objects: Union[list, dict]):
    """Builds a dictionary from object(s)[key_field] to a the object(s)"""
    if isinstance(serialized_objects, list):
        return {obj[key_field]: obj for obj in serialized_objects}
    else:
        return {serialized_objects[key_field]: serialized_objects}


class ReactView(TemplateView):
    """A generic view for rendering a react component in a template with serialized objects available in the context"""

    #: Whether to include the serialized objects from ``get_querysets()`` in ``context[template_state_name]['objects']``
    use_objects_as_state = True

    #: A list of querysets or model instances to include in the context
    querysets = None  # type: List[Union[QuerySet, Model]]

    #: Supplied to ``context[template_state_name]`` if use_objects_as_state is False
    initial_react_state = {}

    #: The filename of the react component to render in the template. This should be the compiled component. The full
    #: path would be: ``static/{react_build_directory}/{app_name}/{react_component}``
    react_component = None

    #: The key in context to store the path to the react component (Should remain 'component' in most cases)
    template_component_name = 'component'

    #: The name of the directory within static that react_components are built to
    react_build_directory = 'dist'

    #: The key in context to store the state passed to redux (Should remain 'props' in most cases)
    template_state_name = 'props'

    #: The field on models to use as key within redux. (Should remain 'url' in most cases)
    key_field = 'url'

    def __init__(self, *args, **kwargs):
        super(ReactView, self).__init__(*args, **kwargs)

    def get_initial_react_state(self):
        """
        Returns the state to pass to redux, override to return something other than serialized objects or
        ``self.initial_react_state``
        """
        if self.use_objects_as_state:
            return self.object_based_react_state()
        else:
            return self.initial_react_state

    def get_react_component(self):
        """
        The path to the react component that can be passed to ``{% static %}`` in the template
        In the form: ``{react_build_directory}/{app_name}/{react_component}``
        """
        return os.path.join(self.react_build_directory, get_app_name(self), self.react_component)

    def get_context_data(self, **kwargs):
        return {
            **super(ReactView, self).get_context_data(**kwargs),
            **self.get_react_context()
        }

    def get_react_context(self):
        """The extra context added to the context to support react & redux"""
        context = {
            self.template_state_name: self.get_initial_react_state(),
            self.template_component_name: self.get_react_component(),
            "key_field": self.key_field
        }
        return context

    def get_querysets(self) -> List[Union[QuerySet, Model]]:
        """Returns ``self.querysets``, override if you need to do more complex processing"""
        return self.querysets

    def object_based_react_state(self):
        """Wraps the serialized objects to be prepared for redux"""
        return {"objects": self.get_serialized_objects()}

    def get_serialized_objects(self):
        """
        Takes the querysets/model instances from ``self.get_querysets()`` and returns their serialized form. Shaped like
        ``{ModelName: {API URL: {the serialized object}}}``
        """

        model_object_pairs = []
        for queryset_or_object in self.get_querysets():
            if isinstance(queryset_or_object, QuerySet):
                serializer = router.get_serializer(queryset_or_object.model)
                model_name = queryset_or_object.model._meta.label_lower
                many = True
            else:
                serializer = router.get_serializer(queryset_or_object._meta.model)
                model_name = queryset_or_object._meta.label_lower
                many = False
            serialized_objects = serializer(queryset_or_object,
                                            context={'request': self.request, 'view': self, 'format': None},
                                            many=many).data
            model_object_pairs.append((model_name, serialized_objects))
        return redux_objects(self.key_field, *model_object_pairs)


ReactMultiModelView = ReactView  # Alias for ReactView to avoid breaking backwards compatibility
