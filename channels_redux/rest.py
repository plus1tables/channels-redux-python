from collections import OrderedDict
from importlib import import_module
from sys import stderr
from typing import Type, Iterable, Callable, Dict

from django.conf import settings
from django.conf.urls import url as urlpattern
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Model, QuerySet
from django.http import HttpRequest
from django.utils.module_loading import autodiscover_modules
from rest_framework import serializers, viewsets
from rest_framework.routers import DefaultRouter, APIRootView
from rest_framework.permissions import BasePermission


class HyperlinkedModelSerializer(serializers.HyperlinkedModelSerializer):
    @staticmethod
    def fix_view_name(field_kwargs: Dict[str, str], model: Type[Model]) -> Dict[str, str]:
        """Fixes the names of the view to properly handle namespacing"""

        if 'view_name' in field_kwargs:
            view_name_suffix = field_kwargs['view_name'].split('-')[-1]
            field_kwargs['view_name'] = "{}:{}-{}".format(settings.API_APP_NAMESPACE,
                                                          get_nested_property(model, RouterClass.BASE_NAME_PATH),
                                                          view_name_suffix)
        return field_kwargs

    def build_url_field(self, field_name, model_class):
        field_class, field_kwargs = super().build_url_field(field_name, model_class)
        return field_class, self.fix_view_name(field_kwargs, self.Meta.model)

    def build_relational_field(self, field_name, relation_info):
        field_class, field_kwargs = super().build_relational_field(field_name, relation_info)
        return field_class, self.fix_view_name(field_kwargs, model=relation_info[1])


class RestAPIRootView(APIRootView):
    """A modified version of the rest_framework.routers.APIRootView that supports nested root views"""

    #: If set overrides the result of ``APIRootView.get_view_name()``
    view_name = 'All Root'

    def get_view_name(self):
        if self.view_name:
            return self.view_name
        else:
            return super(APIRootView, self).get_view_name()


def get_nested_property(obj, path):
    """Safely retrieves the nested property of ``obj[path[0]][path[1]][path[n]]``"""
    current = obj
    for part in path:
        current = getattr(current, part)
    return current


class RestRouter(DefaultRouter):
    """
    An implementation of rest_framework.routers.BaseRouter that supports registering models with default Serializers and
    ViewSets, this router also allows Serializers to be looked up by Model reference.
    """

    APIRootView = RestAPIRootView

    BASE_NAME_PATH = ['_meta', 'label_lower']
    MODEL_NAME = ['_meta', 'verbose_name_plural']
    NAMESPACE_PATH = ['_meta', 'app_label']

    _registered_serializers = {}
    _apps = []
    _urls_by_app = OrderedDict()

    def __init__(self, *args, **kwargs):
        skip_warning = kwargs.pop('skip_warning') or False
        if not skip_warning:
            print("You're instantiating RestRouter, you should probably use channels_redux.rest.router instead")
        super(RestRouter, self).__init__(*args, **kwargs)

    def get_default_base_name(self, viewset: viewsets.ViewSet) -> str:
        """ If `base_name` is not specified, attempt to automatically determine it from the viewset."""
        queryset = getattr(viewset, 'queryset', None)

        assert queryset is not None, '`base_name` argument not specified, and could ' \
                                     'not automatically determine the name from the viewset, as ' \
                                     'it does not have a `.queryset` attribute.'

        return get_nested_property(queryset.model, self.BASE_NAME_PATH)

    def register_model(
            self,
            model: Type[Model],
            serializer: Type[HyperlinkedModelSerializer]=None,
            viewset: Type[viewsets.ModelViewSet]=None,
            fields: Iterable[str]=None,
            exclude_fields: Iterable[str]=None,
            include_fields: Iterable[str]=None,
            queryset: QuerySet=None,
            filter_func: Callable[[QuerySet, HttpRequest], QuerySet]=None,
            url: str=None,
            permissions_classes: Iterable[BasePermission]=None
    ):
        """
        Similar to register() but instead generates a serializer and viewset based on the fields and properties of the
        model.

        :param model: The Model class to register, the only required field in this method
        :param serializer: Replaces the generated serializer if supplied
        :param viewset: Replaces the generated viewset if supplied
        :param fields: A list of field names to include in the serializer, replaces the generated list of fields if
            supplied
        :param exclude_fields: A list of field names to remove from ``fields`` or the generated list of fields if that
            argument is not supplied
        :param include_fields: A list of field names to add to the ``fields`` or the generated list of fields if that
            argument is not supplied
        :param queryset: A queryset that is used to limit what is returned by the viewset. Defaults to
            Model.objects.all()
        :param filter_func: A callable that takes a Queryset, an HttpRequest object and returns a new filtered queryset
        :param url: Overrides the generated url path for this model
        :param permissions_classes: A list of permissions classes to be passed to the ViewSet
        """
        exclude_fields = exclude_fields or set()
        include_fields = include_fields or set()
        fields = self.get_default_fields_for_model(model, fields, exclude_fields, include_fields)

        serializer = serializer or self.get_default_serializer(model, fields)
        self._registered_serializers[model] = serializer
        if not issubclass(serializer, HyperlinkedModelSerializer):
            print("Serializer is not an instance of channels_redux.rest.HyperlinkedModelSerializer "
                  "unexpected behavior may occur", file=stderr)

        queryset = queryset or self.get_default_queryset(model)
        if viewset is None:
            viewset = self.get_default_viewset(queryset, serializer, filter_func, permissions_classes)
        else:
            viewset.serializer_class = serializer  # Ensure that the serializer on the viewset matches the one we expect

        if url is None:
            url = self.get_default_url(model)
        self.register(url, viewset)
        self._add_url(model)

    def _add_url(self, model: Type[Model]):
        app_config = model._meta.app_config
        self._apps.append(app_config)

        existing_urls = self._urls_by_app.get(app_config.verbose_name, OrderedDict())
        existing_urls[self.format_url(get_nested_property(model, self.MODEL_NAME))] = self.list_name(model)
        self._urls_by_app[app_config.verbose_name] = existing_urls

    def list_name(self, model: Type[Model]) -> str:
        """The name of the url for the api list view of the model class"""
        return self.routes[0].name.format(basename=get_nested_property(model, self.BASE_NAME_PATH))

    def get_default_queryset(self, model: Type[Model]) -> QuerySet:
        return model._default_manager.all()

    def get_serializer(self, model: Type[Model]) -> serializers.BaseSerializer:
        """Retrieves the registered serializer for the model class"""
        return self._registered_serializers[model]

    def get_default_url(self, model: Type[Model]) -> str:
        """Creates the url for the api list view of the model class"""
        return '/'.join((
            self.format_url(get_nested_property(model, self.NAMESPACE_PATH)),
            self.format_url(get_nested_property(model, self.MODEL_NAME))
        ))

    def format_url(self, part: str) -> str:
        """Replaces characters that are unsafe for urls in `part`"""
        return part.replace(' ', '-').replace('_', '-')

    def get_default_serializer(self,
                               model_class: Type[Model],
                               serializer_fields: Iterable[str]) -> Type[serializers.HyperlinkedModelSerializer]:
        """Generates a serializer for the model_class exposing serializer_fields"""
        class ModelSerializer(HyperlinkedModelSerializer):
            class Meta:
                model = model_class
                fields = serializer_fields
        return ModelSerializer

    def get_default_viewset(self,
                            qs: QuerySet,
                            serializer: serializers.BaseSerializer,
                            filter_func: Callable[[QuerySet, HttpRequest], QuerySet],
                            permissions: Iterable[Type[BasePermission]]) -> Type[viewsets.ModelViewSet]:
        """
        Generates a viewset for the `qs` queryset using `serializer_class` and `permissions_classes`.
        Also accepts a filter function that can use a request to filter a queryset
        """
        class ModelViewSet(viewsets.ModelViewSet):
            queryset = qs
            serializer_class = serializer
            permission_classes = permissions or tuple()

            def get_queryset(self):
                if filter_func is None:
                    return self.queryset
                return filter_func(self.queryset, self.request)
        return ModelViewSet

    def get_default_fields_for_model(self,
                                     model: Type[Model],
                                     fields: Iterable[str],
                                     exclude_fields: Iterable[str],
                                     include_fields: Iterable[str]) -> Iterable[str]:
        """Gets a tuple of field names for the model

        :param model: The model class to get a list of field names for
        :param fields: see register_model()
        :param exclude_fields: see register_model()
        :param include_fields: see register_model()
        """
        if fields is not None:
            updated_fields = set(fields)
            updated_fields.add('pk')
            updated_fields.add('url')
            return tuple(updated_fields)

        updated_fields = set(include_fields)
        excluded = set(exclude_fields)

        updated_fields.add('url')  # Always include this
        updated_fields |= model._meta._property_names  # This include pk as well as developer defined properties

        for field in model._meta.get_fields():
            if field.auto_created or field.name in excluded:  # Skip reverse relations
                continue
            updated_fields.add(field.name)

        return tuple(updated_fields)

    def get_urls(self):
        extra_urls = []
        for app_config in self._apps:
            root_view = self.APIRootView.as_view(api_root_dict=self._urls_by_app[app_config.verbose_name],
                                                 view_name='{} Root'.format(app_config.verbose_name))

            extra_urls.append(urlpattern(r'^{}/$'.format(self.format_url(app_config.name)),
                                         view=root_view,
                                         name='{}-root'.format(app_config.name)))
        extra_urls.append(urlpattern('^all/$', view=super(RestRouter, self).get_api_root_view(), name='all-root'))
        return super(RestRouter, self).get_urls() + extra_urls

    def get_api_root_view(self, api_urls=None):
        api_root_dict = OrderedDict()
        for app_config in self._apps:
            api_root_dict[app_config.verbose_name] = '{}-root'.format(app_config.name)
        api_root_dict['All'] = 'all-root'
        return self.APIRootView.as_view(api_root_dict=api_root_dict, view_name='Api Root')


def autodiscover():
    """See django.utils.module_loading.autodiscover_modules"""
    autodiscover_modules('rest', register_to=None)


def _get_router_class():
    module_path, class_name = settings.CHANNELS_REDUX_ROUTER_CLASS.rsplit('.', 1)
    try:
        router_module = import_module(module_path)
        return getattr(router_module, class_name)
    except (AttributeError, ImportError) as e:
        raise ImproperlyConfigured(
            'settings.CHANNELS_REDUX_ROUTER_CLASS is improperly configured. Could not find {}. '
            'Please supply a valid class or use the default.'.format(settings.CHANNELS_REDUX_ROUTER_CLASS)
        ) from e


RouterClass = _get_router_class()
router = RouterClass(skip_warning=True)

