#!/usr/bin/env bash

YELLOW='\033[1;33m'
RED='\033[0;31m'
ENDCOLOR='\033[0m'

rm -r build/*
rm -r dist/*
rm -r channels_redux.egg-info/*

python setup.py sdist bdist_wheel

REPO_URL="https://test.pypi.org/legacy/"
if [ "$1" == "--real" ]
then
    REPO_URL="https://upload.pypi.org/legacy/"
fi
twine upload --repository-url $REPO_URL dist/*

RESULT=$?
if [ $RESULT == 0 ]
then
    echo -e "${YELLOW}Finished uploading to ${REPO_URL}${ENDCOLOR}"
else
    echo -e "${RED}Failed to upload to ${REPO_URL}${ENDCOLOR}"
fi
